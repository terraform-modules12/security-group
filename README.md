# AWS VPC Module

  

### What does it do?

This module will allow you to create an AWS Security Group and Rules

- Security Group
- Security Group Rule
- Cidr Rule

### Usage
#### Security group:

	module  "security-group" {
		source                      =  "git@gitlab.com:terraform-modules12/security-group.git//group"
		region                      =  "eu-west-2
		environment                 =  "production"
		service_name                =  "my_service"
		security_group_name         =  "my-service-production-security-group"
		security_group_description  =  "Security group for my-service in production"
		vpc_id                      =  var.vpc_id
	}

### Variable definitions


- region: The region to deploy this Security Group to
- environment: The environment this Security Group belongs to (development/production)
- service_name: The name of the service this Security Group is for (MyWebsite)
- security_group_name: The name of this Security Group
- security_group_description: The description of this Security Group
- vpc_id: The id of the VPC this Security Group belongs to

#### cidr rule:
	module  "http_inbound_cidr_rule" {
		source                      =  "git@gitlab.com:terraform-modules12/security-group.git//rules/cidr"
		region                      =  "eu-west-2
		security_group_id           =  module.security-group.security_group_id
		type                        =  "ingress"
		from_port                   =  80
		to_port                     =  80
		protocol                    =  "tcp"
		cidr_blocks                 =  data.terraform_remote_state.infrastructure.outputs.vpc_cidr_block
		description                 =  "cidr Security Group rule for my-service in production"
	}
### Variable definitions
- region: The region to deploy this Security Group Rule to
- security_group_id: The id of the Security Group to attach this rule to
- type: The type of rule we are creating (ingress/egress)
- from_port: The starting port number
- to_port: the ending port number
- protocol: The protocol to use for this rule (TCP/UDP)
- cidr_blocks: The CIDR range to apply this rule to
- description: The description for this rule

#### security group rule:
		module  "http_inbound_security_group_rule" {
		source                      =  "git@gitlab.com:terraform-modules12/security-group.git//rules/security-group"
		region                      =  var.region
		security_group_id           =  module.security-group.security_group_id
		type                        =  "ingress"
		from_port                   =  80
		to_port                     =  80
		protocol                    =  "tcp"
		source_security_group_id    =  data.terraform_remote_state.this.outputs.source_security_group_id
		description                 =  "Security Group rule for my-service in production"
	}
	
### Variable definitions
- region: The region to deploy this Security Group Rule to
- security_group_id: The id of the Security Group to attach this rule to
- type: The type of rule we are creating (ingress/egress)
- from_port: The starting port number
- to_port: the ending port number
- protocol: The protocol to use for this rule (TCP/UDP)
- cidr_blocks: The CIDR range to apply this rule to
- description: The description for this rule



