##################################################################################################################
#                                                                                                                #
# Outputs are a way to tell Terraform what data is important. This data is outputted when apply is called, and   # 
# can be queried using the terraform output command.                                                             #
#                                                                                                                #
# Further reading: https://learn.hashicorp.com/tutorials/terraform/aws-outputs                                   #
#                                                                                                                #
##################################################################################################################

output "security_group_id" {
  value = aws_security_group_rule.rule.id
}
