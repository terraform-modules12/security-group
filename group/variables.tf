##################################################################################################################
#                                                                                                                #
# Variables define the parameterization of Terraform configurations. Variables can be overridden via the CLI.    #
#                                                                                                                #
# Further reading: https://www.terraform.io/docs/language/values/variables.html                                  #
#                                                                                                                #
##################################################################################################################

variable "region" {
  description = "AWS region name"
}

variable "service_name" {
  description = "The name of the service this Security Group belongs to"
}

variable "environment" {
  description = "The environment e.g. sit, preprod, production"
}

variable "security_group_name" {
  description = "The name of the security group"
}

variable "security_group_description" {
  description = "The description of the security group"
}

variable "vpc_id" {
  description = "The ID of the VPC"
}
