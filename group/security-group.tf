##################################################################################################################
#                                                                                                                #
# aws_security_group provides details about a specific Security Group.                                           #
#                                                                                                                #
# Further reading: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/security_group #
#                                                                                                                #
##################################################################################################################

resource "aws_security_group" "security_group" {
  name        = "${var.service_name}-${var.environment}-security-group"
  description = var.security_group_description
  vpc_id      = var.vpc_id

  tags = {
    Service     = var.service_name
    Environment = var.environment
    Name        = var.security_group_name
    Terraform   = "true"
  }
}
